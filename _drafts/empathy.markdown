---
layout: post
author: "Matti Salokangas"
comments: true
title:  "Empathy"
date:   2016-5-2 00:00:00 -0800
categories: UX
background: '8.jpg'
---

> Empathy is seeing with the eyes of another, listening with the ears of another and feeling with the heart of another.  -- anon

UNDER CONSTRUCTION
