---
layout: post
author: "Matti Salokangas"
comments: true
title:  "Hello World"
date:   2016-3-1 00:00:00 -0800
categories: About
background: '46.jpg'
---

> In the programming world it is tradition to output the message "Hello World" when writing your first program.  This is my "Hello World" post.

## Welcome
Now a days as a front-end developer, in order to stay relevant we must deal with a constantly changing and evolving ecosystem of technology and devices.

I find myself constantly reading articles, pocketing articles to read, skimming my twitter feed and perusing source code on github to see how the legends are coding.

### A little about Usability

I have found my place in the nebulous of UX.  Lucky for me, more and more companies are pushing for "UX" and yet, many companies still struggle with implementing a great user experience.

> Although 73% of you plan to invest in better UX this year, most aren’t quite sure where to start. It all begins with how you approach the research.
[Digital Telepathy](http://www.dtelepathy.com/blog/business/our-first-ever-ux-survey-results)

#### Learning and Sharing

My hope is that I can capture what I am currently learning and share the knowledge.

Thanks for stopping by and be sure to follow me on [twitter](twitter.com/sturdynut) to get the latest updates.
